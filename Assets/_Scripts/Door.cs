using System;
using System.Collections;
using UnityEngine;

public class Door : MonoBehaviour
{
    public static event Action DoorChanged;
    public KeyCode keyCode;
    public AnimationCurve curve;
    public float duration;
    float timer;
    Vector3 startPos;
    Vector3 endPos;
    bool coroutineRunning;
    bool doorOpen;

    void Start()
    {
        startPos = transform.position;
        endPos = transform.position + Vector3.up * 9f;
    }

    void Update()
    {
        if (Input.GetKeyDown(keyCode) && !coroutineRunning)
        {
            StartCoroutine(SwitchDoorCO());
        }
    }

    IEnumerator SwitchDoorCO()
    {
        //if (doorOpen)
        //{
        //    DoorChanged?.Invoke();
        //}
        Vector3 pos1 = doorOpen ? endPos : startPos;
        Vector3 pos2 = doorOpen ? startPos : endPos;
        coroutineRunning = true;
        while (timer < duration)
        {
            float t = timer / duration;
            t = curve.Evaluate(t);
            transform.position = Vector3.Lerp(pos1, pos2, t);
            timer += Time.deltaTime;
            yield return null;
        }
        timer = 0f;
        transform.position = pos2;
        coroutineRunning = false;
        //if (!doorOpen)
        //{
        //    DoorChanged?.Invoke();
        //}
        DoorChanged?.Invoke();
        doorOpen = !doorOpen;
    }


}
