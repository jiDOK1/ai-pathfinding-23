using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Zchfvy.Plus;

public class NavGraph : MonoBehaviour
{
    public bool dontReconstruct;
    public NavGraphVisualizer visualizer;
    public Path curPath;
    public NavNode[] allNodes;
    List<NavEdge>[] adjacentEdges;

    public void ConstructGraph()
    {
        if (dontReconstruct) return;
        allNodes = GetComponentsInChildren<NavNode>();
        adjacentEdges = new List<NavEdge>[allNodes.Length];
        //Debug.Log("Constructing Graph!");
        for (int i = 0; i < allNodes.Length; i++)
        {
            allNodes[i].index = i;
            allNodes[i].name = $"Nav Node ({i})";
            adjacentEdges[i] = allNodes[i].edges;

        }
        //for (int j = 0; j < allNodes.Length; j++)
        //{
        //    Debug.Log(allNodes[j].ToString());
        //    foreach (NavEdge e in adjacentEdges[j])
        //    {
        //        Debug.Log($"from node : {e.fromNode}");
        //        Debug.Log($"to node : {e.toNode}");
        //    }
        //}
    }

    void OnDrawGizmos()
    {
        visualizer.DrawGraph(this);
    }
}

[System.Serializable]
public class NavGraphVisualizer
{
    public bool visualize = true;
    [Range(0f, 1f)]
    public float edgeOffset = 0.2f;
    [Range(0f, 1f)]
    public float arrowHeadSize = 0.3f;
    public float arrowHeadOffset = 0.5f;

    public void DrawGraph(NavGraph graph)
    {
        if (!visualize) return;
        Gizmos.color = Color.green;
        Transform graphTransform = graph.transform;
        foreach (Transform nodeTransform in graphTransform)
        {
            Gizmos.DrawWireCube(nodeTransform.position, new Vector3(0.6f, 0.6f, 0.6f));
            NavNode node = nodeTransform.GetComponent<NavNode>();
            foreach (NavEdge edge in node.edges)
            {
                if (edge.toNode == null) continue;
                Vector3 fromNodePos = edge.fromNode.transform.position;
                Vector3 toNodePos = edge.toNode.transform.position;
                Vector3 dir = (toNodePos - fromNodePos).normalized;
                Vector3 offset = Vector3.Cross(dir, Vector3.up) * edgeOffset;
                fromNodePos += offset;
                toNodePos += offset;
                GizmosPlus.Arrow(fromNodePos, toNodePos - fromNodePos - (dir * arrowHeadOffset), arrowHeadSize, true);
            }
        }
        Gizmos.color = Color.red;
        if (graph.curPath == null) return;
        for (int i = 0; i < graph.curPath.navNodes.Count - 1; i++)
        {
            Vector3 pos1 = graph.curPath.navNodes[i].transform.position;
            Vector3 pos2 = graph.curPath.navNodes[i + 1].transform.position;
            Vector3 dir = (pos2 - pos1).normalized;
            GizmosPlus.Arrow(pos1, pos2 - pos1 - (dir * arrowHeadOffset), arrowHeadSize, true);
        }
    }
}

[CustomEditor(typeof(NavGraph))]
public class NavGraphEditor : Editor
{
    NavNode[] pair = new NavNode[2];

    bool visualize;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        NavGraph graph = (NavGraph)target;

        visualize = graph.visualizer.visualize;
        if (GUILayout.Button("(Re)construct Graph"))
        {
            graph.ConstructGraph();
        }

    }
    protected virtual void OnSceneGUI()
    {
        NavGraph graph = (NavGraph)target;
        if (!visualize) return;

        NavNode[] allNodes = graph.GetComponentsInChildren<NavNode>();

        if (Event.current.type == EventType.KeyDown)
        {
            if (Event.current.keyCode == KeyCode.Escape)
            {
                pair[0] = null;
                pair[1] = null;
            }
        }

        foreach (NavNode node in graph.allNodes)
        {
            if (Handles.Button(node.transform.position, Quaternion.Euler(90f, 0f, 0f), 1f, 1f, Handles.RectangleHandleCap))
            {
                //Debug.Log(node.name);
                if (pair[0] == null)
                {
                    pair[0] = node;
                }
                else if (pair[1] == null)
                {
                    pair[1] = node;
                    bool disconnect = false;
                    for (int i = pair[0].edges.Count - 1; i >= 0; i--)
                    {
                        if (pair[0].edges[i].toNode == pair[1])
                        {
                            pair[0].edges.Remove(pair[0].edges[i]);
                            disconnect = true;
                            break;
                        }
                    }
                    if (!disconnect)
                    {
                        pair[0].edges.Add(new NavEdge(pair[0], pair[1]));
                    }
                    pair[0] = null;
                    pair[1] = null;
                }
            }
        }
    }
}
