using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NavNode : MonoBehaviour
{
    public List<NavEdge> edges = new List<NavEdge>();
    public int index;

    void OnValidate()
    {
        NavGraph graph = GetComponentInParent<NavGraph>();
        graph?.ConstructGraph();
    }
}

[System.Serializable]
public class NavEdge
{
    public NavNode fromNode;
    public NavNode toNode;
    public float cost => Vector3.Distance(fromNode.transform.position, toNode.transform.position);

    public NavEdge(NavNode fromNode, NavNode toNode)
    {
        this.fromNode = fromNode;
        this.toNode = toNode;
    }
}

[CustomEditor(typeof(NavNode))]
public class NavNodeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        NavNode node = (NavNode)target;
        if(GUILayout.Button("Add Edge"))
        {
            node.edges.Add(new NavEdge(node, null));
        }
    }

}
