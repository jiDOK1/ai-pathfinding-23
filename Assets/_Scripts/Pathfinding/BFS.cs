using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BFS : MonoBehaviour
{
    public NavNode start;
    public NavNode end;

    public bool GetPath(Path path)
    {
        Dictionary<NavNode, NavNode> cameFrom = new Dictionary<NavNode, NavNode>();
        Queue<NavNode> frontier = new Queue<NavNode>();

        frontier.Enqueue(start);
        NavNode curNode = null;

        while (frontier.Count > 0)
        {
            curNode = frontier.Dequeue();
            if (curNode == end) break;

            foreach (NavEdge edge in curNode.edges)
            {
                if (!cameFrom.ContainsKey(edge.toNode))
                {
                    frontier.Enqueue(edge.toNode);
                    cameFrom.Add(edge.toNode, curNode);
                }
            }
        }
        if (curNode != end) return false;

        while(curNode != start)
        {
            path.Add(curNode);
            curNode = cameFrom[curNode];
        }
        path.Add(start);
        path.Reverse();
        GetComponent<NavGraph>().curPath = path;
        return true;
    }
}

[CustomEditor(typeof(BFS))]
public class BFSEditor : Editor
{
    public override void OnInspectorGUI()
    {
        BFS bfs = (BFS)target;
        DrawDefaultInspector();
        if(GUILayout.Button("Calculate Path"))
        {
            //List<NavNode> path = new List<NavNode>();
            Path path = new Path();
            if (bfs.GetPath(path))
            {
                foreach (NavNode node in path.navNodes)
                {
                    Debug.Log(node.index);
                }
            }
            else
            {
                Debug.Log("No complete Path found!");
            }
        }
    }
}
