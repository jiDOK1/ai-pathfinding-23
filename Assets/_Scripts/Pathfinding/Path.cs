using System.Collections.Generic;
using UnityEngine;

public class Path
{
    //INFO: Zwei Alternativen: corners als Liste oder Vector3[] Corners() als Methode

    public List<NavNode> navNodes = new List<NavNode>();
    public List<Vector3> cornersList = new List<Vector3>();
    public Vector3[] corners;

    //public Vector3[] Corners()
    //{
    //    Vector3[] c = new Vector3[navNodes.Count];
    //    for (int i = 0; i < navNodes.Count; i++)
    //    {
    //        c[i] = navNodes[i].transform.position;
    //    }

    //    return c;
    //}

    public void Add(NavNode node)
    {
        navNodes.Add(node);
        cornersList.Add(node.transform.position);
        corners = cornersList.ToArray();
    }

    public void Reverse()
    {
        navNodes.Reverse();
        cornersList.Reverse();
        corners = cornersList.ToArray();
    }
}
