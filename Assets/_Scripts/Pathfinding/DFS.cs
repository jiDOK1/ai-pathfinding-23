using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DFS : MonoBehaviour
{
    public NavNode start;
    public NavNode end;

    public bool GetPath(Path path)
    {
        Dictionary<NavNode, NavNode> cameFrom = new Dictionary<NavNode, NavNode>();
        Stack<NavNode> frontier = new Stack<NavNode>();

        frontier.Push(start);
        NavNode curNode = null;

        while (frontier.Count > 0)
        {
            curNode = frontier.Pop();
            if (curNode == end) break;

            foreach (NavEdge edge in curNode.edges)
            {
                if (!cameFrom.ContainsKey(edge.toNode))
                {
                    frontier.Push(edge.toNode);
                    cameFrom.Add(edge.toNode, curNode);
                }
            }
        }

        if (curNode != end) return false;

        while (curNode != start)
        {
            path.Add(curNode);
            curNode = cameFrom[curNode];
        }

        path.Add(start);
        path.Reverse();
        GetComponent<NavGraph>().curPath = path;
        return true;
    }
}





[CustomEditor(typeof(DFS))]
public class DFSEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DFS dfs = (DFS)target;
        DrawDefaultInspector();

        if (GUILayout.Button("Calculate Path"))
        {
            Path path = new Path();

            if (dfs.GetPath(path))
            {
                foreach (NavNode node in path.navNodes)
                {
                    Debug.Log(node.index);
                }
            }
            else
            {
                Debug.Log("No complete Path found!");
            }
        }
    }
}