using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Priority_Queue;

public class AStar : MonoBehaviour
{
    [SerializeField] NavNode startNode;
    [SerializeField] NavNode endNode;

    public bool GetPath(Path path)
    {
        SimplePriorityQueue<NavNode> openSet = new SimplePriorityQueue<NavNode>();
        HashSet<NavNode> closedSet = new HashSet<NavNode>();
        Dictionary<NavNode, NavNode> cameFrom = new Dictionary<NavNode, NavNode>();
        Dictionary<NavNode, float> gScore = new Dictionary<NavNode, float>();

        openSet.Enqueue(startNode, 0f);
        gScore.Add(startNode, 0f);

        NavNode curNode = null;

        while (openSet.Count > 0)
        {
            curNode = openSet.Dequeue();
            if (curNode == endNode) { break; }
            closedSet.Add(curNode);
            for (int i = 0; i < curNode.edges.Count; i++)
            {
                NavEdge edge = curNode.edges[i];
                if (closedSet.Contains(edge.toNode)) { continue; }
                float tempG = gScore[curNode] + edge.cost;
                float tempF = tempG + GetHeuristic(edge.toNode);
                if (!openSet.Contains(edge.toNode))
                {
                    openSet.Enqueue(edge.toNode, tempF);
                }
                else if (tempG > gScore[edge.toNode]) { continue; }
                cameFrom[edge.toNode] = curNode;
                gScore[edge.toNode] = tempG;
            }
        }
        if(curNode != endNode) { return false; }

        while (curNode != startNode)
        {
            path.Add(curNode);
            curNode = cameFrom[curNode];
        }
        path.Add(startNode);
        path.Reverse();
        GetComponent<NavGraph>().curPath = path;
        return true;
    }

    float GetHeuristic(NavNode node)
    {
        return Vector3.Distance(node.transform.position, endNode.transform.position);
    }
}

[CustomEditor(typeof(AStar))]
public class AStarEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        AStar astar = (AStar)target;
        if(GUILayout.Button("Find Path"))
        {
            Path path = new Path();
            if (astar.GetPath(path))
            {
                Debug.Log("A* found Path:");
                foreach (NavNode node in path.navNodes)
                {
                    Debug.Log(node.index);
                }
            }
            else
            {
                Debug.Log("A*: No complete path found");
            }
        }
    }
}
