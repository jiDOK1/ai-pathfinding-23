using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Priority_Queue;

public class Dijkstra : MonoBehaviour
{
    public NavNode start;
    public NavNode end;


    public bool GetPath(Path path)
    {
        SimplePriorityQueue<NavNode> frontier = new SimplePriorityQueue<NavNode>();
        Dictionary<NavNode, NavNode> cameFrom = new Dictionary<NavNode, NavNode>();
        Dictionary<NavNode, float> costSoFar = new Dictionary<NavNode, float>();

        frontier.Enqueue(start, 0f);
        cameFrom[start] = null;
        costSoFar[start] = 0f;
        NavNode curNode = null;

        while (frontier.Count > 0)
        {
            curNode = frontier.Dequeue();
            Debug.Log(curNode.name);
            if (curNode == end) break;
            foreach (NavEdge edge in curNode.edges)
            {
                float newCost = costSoFar[curNode] + edge.cost;
                if (!costSoFar.ContainsKey(edge.toNode) || newCost < costSoFar[edge.toNode])
                {
                    costSoFar[edge.toNode] = newCost;
                    frontier.Enqueue(edge.toNode, newCost);
                    cameFrom[edge.toNode] = curNode;
                }
            }
        }
        if (curNode != end) return false;

        while (curNode != start)
        {
            path.Add(curNode);
            curNode = cameFrom[curNode];
        }
        path.Add(start);
        path.Reverse();
        GetComponent<NavGraph>().curPath = path;
        return true;
    }
}

[CustomEditor(typeof(Dijkstra))]
public class DijkstraEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Dijkstra dijkstra = (Dijkstra)target;
        DrawDefaultInspector();
        if (GUILayout.Button("Calculate Path"))
        {
            Path path = new Path();
            if (dijkstra.GetPath(path))
            {
                foreach (NavNode node in path.navNodes)
                {
                    Debug.Log(node.index);
                }
            }
            else
            {
                Debug.Log("No complete Path found!");
            }
        }
    }
}
