using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Steering))]
public class NavMeshSteering : MonoBehaviour
{
    public Transform target;
    public float mass = 1f;
    public float maxSpeed = 10f;
    public float maxForce = 5f;
    private NavMeshPath path;
    //string pathStatus;
    Vector3 velocity;
    CharacterController controller;
    Steering steering;
    bool betweenLastTwoWaypoints;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        steering = GetComponent<Steering>();
        path = new NavMeshPath();
        NavMesh.CalculatePath(transform.position + Vector3.down, target.position, NavMesh.AllAreas, path);
        Door.DoorChanged += CalculatePath;
        steering.LastTwo += BetweenLastTwoWaypoints;
    }

    void Update()
    {
        // Update the way to the goal every second.
        for (int i = 0; i < path.corners.Length - 1; i++)
            Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);

        if (path.status == NavMeshPathStatus.PathInvalid || path.corners.Length == 0) return;
        Vector3 steer = new Vector3();
        if (betweenLastTwoWaypoints && Vector3.Distance(transform.position, path.corners[path.corners.Length - 1]) < 3f)
        {
            steer = steering.Arrival(path.corners[path.corners.Length - 1], maxSpeed, maxForce, velocity, 1f, 0.5f);
        }
        else
        {
            steer = steering.WalkWaypoints(path.corners, maxSpeed, maxForce, velocity, 1f);
            Path p = new Path();
            //steer = steering.WalkWaypoints(p.Corners(), maxSpeed, maxForce, velocity, 1f);
            steer = steering.WalkWaypoints(p.corners, maxSpeed, maxForce, velocity, 1f);
        }
        Move(steer);
    }

    //private void OnGUI()
    //{
    //    GUILayout.Label(path.corners.Length.ToString());
    //}

    protected void Move(Vector3 steering)
    {
        Vector3 acceleration = steering / mass;
        velocity += acceleration;
        velocity = Vector3.ClampMagnitude(velocity, maxSpeed);
        controller.Move(velocity * Time.deltaTime);
    }

    public void BetweenLastTwoWaypoints()
    {
        betweenLastTwoWaypoints = true;
    }

    // recalculate path when level structure changed
    public void CalculatePath()
    {
        betweenLastTwoWaypoints = false;
        path.ClearCorners();
        NavMesh.CalculatePath(transform.position + Vector3.down, target.position, NavMesh.AllAreas, path);
        //Debug.Log("recalculating the path");
        //Debug.Log(path.status);
    }
}