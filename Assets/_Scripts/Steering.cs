using System;
using UnityEngine;

public class Steering : MonoBehaviour
{
    int curWaypointIdx;
    public event Action LastTwo;
    // Debug
    int curWaypointsLength;
    string curSteering;
    string distanceToLast;

    // Seek
    public Vector3 Seek(Vector3 targetPos, float maxSpeed, float maxForce, Vector3 velocity)
    {
        // get dir to target
        Vector2 desiredVelocity = (targetPos.ToVector2() - transform.position.ToVector2()).normalized * maxSpeed;
        // do normal steering calculations
        Vector2 steering = desiredVelocity - velocity.ToVector2();
        steering = Vector2.ClampMagnitude(steering, maxForce);
        return steering.ToVector3();
    }

    // Flee
    public Vector3 Flee(Vector3 targetPos, float maxSpeed, float maxForce, Vector3 velocity)
    {
        // get dir to target and mirror it by setting it to negative
        Vector2 desiredVelocity = -(targetPos.ToVector2() - transform.position.ToVector2()).normalized * maxSpeed;
        // do normal steering calculations
        Vector2 steering = desiredVelocity - velocity.ToVector2();
        steering = Vector2.ClampMagnitude(steering, maxForce);
        return steering.ToVector3();
    }

    // Arrival
    public Vector3 Arrival(Vector3 targetPos, float maxSpeed, float maxForce, Vector3 velocity, float slowDist, float stopDist)
    {
        curSteering = "arrival";
        // get direction to target
        Vector2 dirToTarget = targetPos.ToVector2() - transform.position.ToVector2();
        // get distance (length of dir-vector)
        float distance = dirToTarget.magnitude;
        // limit distance to be shorter (subtract stop distance)
        distance = distance - stopDist;
        // limit direction vector to the new, shorter distance (length)
        dirToTarget = Vector2.ClampMagnitude(dirToTarget, distance);
        // get t value for lerping:
        // when distance >= slowDist -> values over 1
        // when distance < slowDist -> values below 1
        float t = distance / slowDist;
        // get desired velocity from dir vector varying by how near we are to target - starts varying at slow dist
        // values for t over 1 act like 1, because Lerp is clamped - for those we get maxSpeed
        Vector2 desiredVelocity = Mathf.Lerp(0f, maxSpeed, t) * dirToTarget.normalized;
        // do the normal steering calculations
        Vector2 steering = desiredVelocity - velocity.ToVector2();
        steering = Vector2.ClampMagnitude(steering, maxForce);
        return steering.ToVector3();
    }


    // Waypoints/Patrolling
    public Vector3 WalkWaypoints(Vector3[] waypoints, float maxSpeed, float maxForce, Vector3 velocity, float nearDist)
    {
        curSteering = "walk waypoints";
        // safety check if waypoints change mid-steering
        if (curWaypointIdx >= waypoints.Length) curWaypointIdx = 0;
        curWaypointsLength = waypoints.Length;

        // check if curWaypoint is reached - if yes switch to next waypoint
        if (Vector3.Distance(waypoints[curWaypointIdx], transform.position) < nearDist)
        {
            // invoke event when between last two waypoints
            curWaypointIdx++;
            if (curWaypointIdx == waypoints.Length - 1)
            {
                LastTwo?.Invoke();
            }
            if (curWaypointIdx >= waypoints.Length) curWaypointIdx = 0;
        }
        // get current target from waypoints
        Vector3 targetPos = waypoints[curWaypointIdx];
        // get dir to current target
        Vector2 desiredVelocity = (targetPos.ToVector2() - transform.position.ToVector2()).normalized * maxSpeed;
        // do normal steering calculations
        Vector2 steering = desiredVelocity - velocity.ToVector2();
        steering = Vector2.ClampMagnitude(steering, maxForce);
        return steering.ToVector3();
    }

    // Separation
    public Vector3 Separation(Transform[] avoidees, float maxSpeed, float maxForce, Vector3 velocity, float minDist)
    {
        // reset desired velocity
        Vector2 desiredVelocity = Vector2.zero;
        // local variable to track the number of avoidees that are near enough
        int count = 0;
        // loop over all potential avoidees
        for (int i = 0; i < avoidees.Length; i++)
        {
            // flee vector for one avoidee
            Vector2 desiredSeparation = transform.position.ToVector2() - avoidees[i].position.ToVector2();
            // get distance to avoidee
            float distance = desiredSeparation.magnitude;
            // if distance too far stop here and continue with next iteration
            if (distance > minDist && avoidees[i] == transform) continue;
            // limit flee vector to length 1
            desiredSeparation.Normalize();
            // add flee vector to desired velocity
            desiredVelocity += desiredSeparation;
            // increment number of avoidees
            count++;
        }
        // divide desired velocity by count
        desiredVelocity /= count;
        // make magnitude of desired velocity maxSpeed
        desiredVelocity *= maxSpeed;
        // do normal steering calculations
        Vector2 steering = desiredVelocity - velocity.ToVector2();
        steering = Vector2.ClampMagnitude(steering, maxForce);
        return steering.ToVector3();
    }

    void OnGUI()
    {
        GUILayout.Label(curWaypointIdx.ToString());
        GUILayout.Label(curWaypointsLength.ToString());
        GUILayout.Label(curSteering);
        GUILayout.Label(distanceToLast);
    }
}
