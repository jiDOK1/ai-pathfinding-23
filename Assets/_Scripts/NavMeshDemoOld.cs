using UnityEngine;
using UnityEngine.AI;

public class NavMeshDemoOld : MonoBehaviour
{
    public Transform goal;
    NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            agent.SetDestination(goal.position);
        }
    }
}
